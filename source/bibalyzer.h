/* SPDX-License-Identifier: GPL-2.0 */
/*
 * bibalyzer.c: Bit-bang data logger
 *
 * Copyright (C) 2018 Stefan Mavrodiev <stefan.mavrodiev@gmail.com>
 */

#include <linux/serial_core.h>
#include <linux/of_device.h>

/* Define GPIO */
struct biba_gpios {
	const char 		*label;
	struct gpio_desc 	*gpiod;
};

struct biba_priv {
	// struct uart_port	port;
	// struct device		*dev;
	struct tty_driver 	*tty_drv;

	// struct hrtimer		timer;
	// ktime_t			ts;

	// struct task_struct	*kthread;

	int 			num_gpios;
	struct biba_gpios	gpios[];

};


// int biba_uart_init(void);
int biba_uart_init(struct platform_device *pdev);
void biba_uart_exit(struct platform_device *pdev);
int biba_uart_assign(void);
int biba_uart_release(struct device *dev);
