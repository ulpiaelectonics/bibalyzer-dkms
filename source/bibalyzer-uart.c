// SPDX-License-Identifier: GPL-2.0
/*
 * bibalyzer.c: Bit-bang data logger
 *
 * Copyright (C) 2018 Stefan Mavrodiev <stefan.mavrodiev@gmail.com>
 */
#define DEBUG
#include <linux/serial_core.h>
#include <linux/of_device.h>
// #include <linux/console.h>
#include <linux/tty.h>
#include <linux/tty_flip.h>

#include "bibalyzer.h"

#if 0
#define BIBA_SERIAL_NAME "ttyBIBA"

static struct uart_driver biba_uart_driver;

/* -----------------------------------------------------------------------------
 * Console opts
 */
static void biba_console_write(struct console *co, const char *s, unsigned cnt)
{
	// struct biba_priv *priv = &co->data;
}

static int biba_console_setup(struct console *co, char *options)
{
	return 0;
}

static struct console biba_console = {
	.name		= BIBA_SERIAL_NAME,
	.write          = biba_console_write,
	.device         = uart_console_device,
	.setup          = biba_console_setup,
	.flags          = CON_PRINTBUFFER,
	.index		= -1,
	.data		= &biba_uart_driver,
};

static struct uart_driver biba_uart_driver = {
	.driver_name	= "bibalyzer",
	.dev_name	= BIBA_SERIAL_NAME,
	.major		= 0,
	.minor		= 0,
	.nr		= 1,
	.cons		= &biba_console,
};

int biba_uart_assign(void)
{

}

int biba_uart_release(struct device *dev)
{
	struct biba_priv *priv = dev_get_drvdata(dev);
	int ret = 0;

	if (priv->port) {
		dev_dbg(dev, "Removing port: %s\n", priv->port.name);
		ret = uart_remove_one_port(&biba_uart_driver, &priv->port);
	}

	return ret;
}
#endif

static int biba_tty_install(struct tty_driver *driver, struct tty_struct *tty)
{
	dev_dbg(tty->dev, "%s()\n", __func__);
	return 0;
}

static int biba_tty_open(struct tty_struct *tty, struct file *filp)
{
	return 0;
}

static void biba_tty_close(struct tty_struct *tty, struct file *filp)
{

}

static int biba_tty_write(struct tty_struct *tty, const unsigned char *buf,
                                                                    int len)
{
	return 0;
}

static int biba_tty_write_room(struct tty_struct *tty)
{
	return 0;
}


static int biba_tty_chars_in_buffer(struct tty_struct *tty)
{
	return 0;
}

static void biba_tty_flush_buffer(struct tty_struct *tty)
{

}

static int biba_tty_ioctl(struct tty_struct *tty,
                        unsigned int cmd, unsigned long arg)
			{
				return 0;

			}

static void biba_tty_throttle(struct tty_struct *tty)
{

}

static void biba_tty_unthrottle(struct tty_struct *tty)
{

}

static void biba_tty_set_termios(struct tty_struct *tty, struct ktermios *old)
{
}

static void biba_tty_hangup(struct tty_struct *tty)
{

}

static void biba_tty_wait_until_sent(struct tty_struct *tty, int timeout)
{

}

static int biba_tty_tiocmget(struct tty_struct *tty)
{
	return 0;
}

static int biba_tty_tiocmset(struct tty_struct *tty, unsigned int set,
			     unsigned int clear)
{
	return 0;
}


static int biba_tty_break_ctl(struct tty_struct *tty, int state)
{
	return 0;
}


static void biba_tty_cleanup(struct tty_struct *tty)
{

}


static const struct tty_operations biba_tty_ops = {
	.install                = biba_tty_install,
	.open                   = biba_tty_open,
	.close                  = biba_tty_close,
	.write                  = biba_tty_write,
	.write_room             = biba_tty_write_room,
	.chars_in_buffer        = biba_tty_chars_in_buffer,
	.flush_buffer           = biba_tty_flush_buffer,
	.ioctl                  = biba_tty_ioctl,
	.throttle               = biba_tty_throttle,
	.unthrottle             = biba_tty_unthrottle,
	.set_termios            = biba_tty_set_termios,
	.hangup                 = biba_tty_hangup,
	.wait_until_sent        = biba_tty_wait_until_sent,
	.tiocmget               = biba_tty_tiocmget,
	.tiocmset               = biba_tty_tiocmset,
	.break_ctl              = biba_tty_break_ctl,
	.cleanup                = biba_tty_cleanup,
};



/* -----------------------------------------------------------------------------
 * UART init/deinit
 */
/* drivers/mmc/core/sdio_uart.c  */
int biba_uart_init(struct platform_device *pdev)
{
	struct biba_priv *priv = platform_get_drvdata(pdev);
	int ret;

	priv->tty_drv = tty_alloc_driver(1,
					 TTY_DRIVER_RESET_TERMIOS |
					 TTY_DRIVER_REAL_RAW |
					 TTY_DRIVER_UNNUMBERED_NODE);
	if (IS_ERR(priv->tty_drv)) {
		dev_err(&pdev->dev, "Counldn't allocate memory for tty driver!\n");
		return PTR_ERR(priv->tty_drv);
	}

	priv->tty_drv->driver_name = "bibalyzer";
	priv->tty_drv->name = "ttyBIBA";
	priv->tty_drv->major = 0;
	priv->tty_drv->minor_start = 0;
	priv->tty_drv->subtype = SERIAL_TYPE_NORMAL;
	priv->tty_drv->flags = TTY_DRIVER_REAL_RAW | TTY_DRIVER_UNNUMBERED_NODE;
	priv->tty_drv->init_termios = tty_std_termios;
	tty_set_operations(priv->tty_drv, &biba_tty_ops);

	ret = tty_register_driver(priv->tty_drv);
	if (ret) {
		put_tty_driver(priv->tty_drv);
		return ret;
	}

	dev_info(&pdev->dev, "ttyBIBA registered as %d,%d.\n",
		 priv->tty_drv->major, priv->tty_drv->minor_start);

	return 0;
	// return uart_register_driver(&biba_uart_driver);
}

void biba_uart_exit(struct platform_device *pdev)
{
	struct biba_priv *priv = platform_get_drvdata(pdev);

	tty_unregister_driver(priv->tty_drv);
	put_tty_driver(priv->tty_drv);
	// uart_unregister_driver(&biba_uart_driver);
}
