// SPDX-License-Identifier: GPL-2.0
/*
 * bibalyzer.c: Bit-bang data logger
 *
 * Copyright (C) 2018 Stefan Mavrodiev <stefan.mavrodiev@gmail.com>
 */

#include <linux/module.h>
#include <linux/of_device.h>
#include <linux/serial_core.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/property.h>
#include <linux/err.h>
#include <linux/gpio.h>
#include <linux/gpio/consumer.h>
#include <linux/hrtimer.h>

#include "bibalyzer.h"





static const struct of_device_id biba_of_match[] = {
	{ .compatible = "bibalyzer", },
	{},
};
MODULE_DEVICE_TABLE(of, biba_of_match);

static int biba_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct fwnode_handle *child;
	struct biba_priv *priv;
	int count, ret;

	count = device_get_child_node_count(dev);
	if (!count) {
		dev_err(dev, "There is no chuld nodes, describing input ports!\n");
		return -ENODEV;
	}

	priv = devm_kzalloc(dev, sizeof(struct biba_priv) +
			    sizeof(struct biba_gpios) * count, GFP_KERNEL);
	if (!priv) {
		dev_err(dev, "Out of memory!\n");
		return -ENOMEM;
	}
	platform_set_drvdata(pdev, priv);

	device_for_each_child_node(dev, child) {
		struct biba_gpios *gpio = &priv->gpios[priv->num_gpios];
		struct device_node *np = to_of_node(child);

		ret = fwnode_property_read_string(child, "label", &gpio->label);
		if (ret && np)
			gpio->label = np->name;
		if (!gpio->label) {
			fwnode_handle_put(child);
			return -EINVAL;
		}

		gpio->gpiod = devm_fwnode_get_gpiod_from_child(dev, NULL, child,
							       GPIOD_IN,
							       gpio->label);
		if (IS_ERR(gpio->gpiod)) {
			fwnode_handle_put(child);
			return ERR_CAST(gpio->gpiod);
		}

		dev_info(dev, "Registed gpio input: %s\n", gpio->label);
		priv->num_gpios++;
	}

	dev_info(dev, "Registered total %d inputs.\n", priv->num_gpios);

	biba_uart_init(pdev);

	return 0;
}

static int biba_remove(struct platform_device *pdev)
{
	biba_uart_exit(pdev);
	platform_set_drvdata(pdev, NULL);

	return 0;
}

static struct platform_driver biba_driver= {
	.probe = biba_probe,
	.remove = biba_remove,
	.driver = {
		.name = "bibalyzer",
		.of_match_table = biba_of_match,
	}
};

static int __init biba_init(void)
{
	int ret;

	ret = platform_driver_register(&biba_driver);
	if (ret) {
		pr_err("Failed to register bibalyzer: %d\n", ret);
		return ret;
	}
	return 0;
}

static void __exit biba_exit(void)
{
	platform_driver_unregister(&biba_driver);
}

module_init(biba_init);
module_exit(biba_exit);

MODULE_AUTHOR("Stefan Mavrodiev <stefan.mavrodiev@gmail.com>");
MODULE_DESCRIPTION("Bit-bang logic analyzer driver");
MODULE_LICENSE("GPL v2");
MODULE_ALIAS("platform:bibalyzer");
