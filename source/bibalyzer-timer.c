// SPDX-License-Identifier: GPL-2.0
/*
 * bibalyzer.c: Bit-bang data logger
 *
 * Copyright (C) 2018 Stefan Mavrodiev <stefan.mavrodiev@gmail.com>
 */
#include <linux/hrtimer.h>

#include "bibalyzer.h"

static enum hrtimer_restart biba_timer_cb(struct hrtimer *timer)
{
	struct biba_priv *priv = container_of(timer, struct biba_priv, timer);
	ktime_t now = timer->base->get_time();
 	pr_info("ktime: %lld\n", ktime_to_ns(now));

	return HRTIMER_RESTART;
}

static int biba_start_timer(void)
{
	return 0;
}
EXPORT_SYMBOL_GPL(biba_start_timer);
